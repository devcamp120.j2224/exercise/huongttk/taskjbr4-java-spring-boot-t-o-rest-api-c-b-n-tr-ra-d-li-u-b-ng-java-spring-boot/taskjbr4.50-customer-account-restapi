package com.jbr40.customeraccountapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerAccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList <Account> getAccountList(){
        ArrayList <Account> listAccount = new ArrayList<>();
        Customer customer1 = new Customer(10, "Huong", 5);
        Customer customer2 = new Customer(11, "Duy", 10);
        Customer customer3 = new Customer(12, "Hien", 15);

        System.out.println("customer1 = " + customer1);
        System.out.println("customer2 = " + customer2);
        System.out.println("customer3 = " + customer3);

        Account account1 = new Account(1, customer1, 4500);
        Account account2 = new Account(2, customer2, 5000);
        Account account3 = new Account(3, customer3, 1000);

        System.out.println("account1 = " + account1);
        System.out.println("account2 = " + account2);
        System.out.println("account3 = " + account3);

        listAccount.add(account1);
        listAccount.add(account2);
        listAccount.add(account3);

        return listAccount;
    }
}
