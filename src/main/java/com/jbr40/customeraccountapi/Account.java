package com.jbr40.customeraccountapi;

public class Account {
    private int id = 0;
    private Customer customer;
    private double balance;
    
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [balance=" + balance + ", customer=" + customer + ", id=" + id + "]";
    }

    public String getCustomerName() {
        return customer.getName();
    }
    public Account deposit(double amount){
        this.balance += amount;
        return this;
    }
    public Account withdraw(double amount){
        if (this.balance >= amount){
            this.balance -= amount;
        }else {
            System.out.println("amount withdraw exceeds the current balance!");
        }
        return this;
    }
}
